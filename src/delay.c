#include "delay.h"

void delay_ms( int ms)
{
	volatile long unsigned int i = ms*30;
	while(i) {
		i--;
	}
}

void delay(unsigned int generic_unit) {
	volatile unsigned int i = generic_unit;
	while(i) {
		i--;
	}
}

