#ifndef LED_H
#define LED_H
#include <stdint.h>

void LED_set_led(uint8_t index, uint8_t r, uint8_t g, uint8_t b);
void LED_send_led();


#endif
