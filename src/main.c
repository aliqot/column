//#define F_CPU 20000000UL
#include <avr/io.h>

#include "led.h"
#include "modport.h"
#include "sr_switch.h"

int update_flag = 1;

int sw = 0;
int p_sw = 0;
int change_sw = 0;

char data_l = 0;
char p_data_l = 0;
char change_l = 0;

char data_r = 0;
char p_data_r = 0;
char change_r = 0;

int occasional = 0;

void contact_minion(char byte) {
	data_r = MODPORT_contact_minion(byte);
	if ( data_r ) {
		if ( (change_r = data_r ^ p_data_r) ) {
			for (int ea_sw = 0; ea_sw < 4; ea_sw++) {
				if (change_r & data_r & (1<<ea_sw)) {
					LED_set_led(ea_sw, 0xFF,0,0);
				}
				else if (change_r & (~data_r & (1<<ea_sw))) {
					LED_set_led(ea_sw, 0,0,0);
				}
			}
		}
		p_data_r = data_r;
	}
}

int main(void)
{
	MODPORT_init();
	DDRA 	|= 0b00001110;
//	PORTA	|= 0b1;
//	DDRA	&=~0b10;
	DDRB	|= 0b11;
	PORTB	|= 0b1;
	for (int ea_led = 0; ea_led < 4; ea_led++) {
		LED_set_led(ea_led, 0,0,0);
	}
	while(1){
		if (update_flag || (occasional > 10)) {
			if (!update_flag) {
				contact_minion(0);
			}
			update_flag = 0;
			occasional = 0;
			LED_send_led();
		}
		sw = read_switch();
		if ( (change_sw = sw ^ p_sw) ) {
			for (int ea_sw = 0; ea_sw < 4; ea_sw++) {
				if (change_sw & sw & (1<<ea_sw)) {
					LED_set_led(ea_sw, 0,0xFF,0);
				}
				else if (change_sw & (~sw & (1<<ea_sw))) {
					LED_set_led(ea_sw, 0,0,0);
				}
			}
			contact_minion(sw+0x80);
			MODPORT_add_to_queue(sw+0x80);
			update_flag = 1;
		}
		p_sw = sw;
		if (MODPORT_is_poppable()) {
			data_l = MODPORT_pop();
			if ( data_l ) {
				if ( (change_l = data_l ^ p_data_l) ) {
					for (int ea_sw = 0; ea_sw < 4; ea_sw++) {
						if (change_l & data_l & (1<<ea_sw)) {
							LED_set_led(ea_sw, 0,0,0xff);
						}
						else if (change_l & (~data_l & (1<<ea_sw))) {
							LED_set_led(ea_sw, 0,0,0);
						}
					}
				}
				p_data_l = data_l;
			}
		}
		occasional++;
	}
}
