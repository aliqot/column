#include "modport.h"

#include <avr/io.h>
#include <avr/interrupt.h>

#include "delay.h"
#include "led.h"

#define SPI_STC_vect _VECTOR(20)
#define DDR_SPI DDRA
#define DD_MISO 5
#define DD_MOSI 6
#define DD_SCK 4
#define DD_SS 7

#define MAX_QUEUE_LEN 128
char _queue[MAX_QUEUE_LEN];
int _queue_pos = 0;
int _queue_len = 0;
#define MAX_DATA_IN_LEN 128
char _data_in[MAX_DATA_IN_LEN];
int _data_in_pos;
int _data_in_len;

void MODPORT_init(void) {
	/* Set MISO output, all others input */
	DDR_SPI = (1<<DD_MISO);
	PUEA |= ((1<<DD_MOSI)|(1<<DD_SCK)|(1<<DD_SS));
	/* Enable SPI */
	SPCR = ((1<<SPE)|(1<<SPIE)|(1<<CPOL)|(1<<CPHA));
	SPDR = 0;
//	SREG |= 1<<7;
	sei();

	_data_in_pos = 0;
	_data_in_len = 0;

	_queue_pos = 0;
	_queue_len = 0;
}

void byte_to_led(char byte, uint8_t r,uint8_t g,  uint8_t b){
	for (int i = 0; i < 4; i++) {
		if (byte&(1<<i)) { LED_set_led(i, r,g,b); }
		else { LED_set_led(i, 0,0,0); }
	}
}


ISR(SPI_STC_vect){
//	LED_set_led(0, 0xff, 0, 0);
//	PORTA &=~(1<<DD_MISO);
	if (SPDR && (_data_in_len < MAX_DATA_IN_LEN)) {
		_data_in[(_data_in_pos+_data_in_len)&(MAX_DATA_IN_LEN-1)] = SPDR;
//		_data_in_pos = (_data_in_pos + 1) & (MAX_DATA_IN_LEN - 1);
		_data_in_len++;
	}
	if (_queue_len) {
		SPDR = _queue[_queue_pos];
		_queue_pos = (_queue_pos + 1) & (MAX_DATA_IN_LEN-1);
		_queue_len--;
	}
	else {
		SPDR = 0;
	}

}

int MODPORT_is_poppable() {
	return _data_in_len;
}
int _output = 0;
int MODPORT_pop() {
	if (_data_in_len) {
		_output = _data_in[_data_in_pos];
		_data_in_pos = (_data_in_pos + 1) & (MAX_DATA_IN_LEN - 1);
		_data_in_len--;
		return _output;
	}
	return 0;
}

int MODPORT_add_to_queue(char byte){
	if (!byte) {
		return 0;
	}
	if (_queue_len >= MAX_QUEUE_LEN) {
		return 1;
	}
	_queue[(_queue_pos+_queue_len)&(MAX_QUEUE_LEN-1)] = byte;
	_queue_len++;
//	PORTA |= 1<<DD_MISO;
	return 0;
}

int MODPORT_is_minion_ready() {
	PORTB	&= 0b1100; // MUX: 00
	if (PINA & 0b1) {
		return 1;
	}
	return 0;
}

char MODPORT_contact_minion(char byte) {
	PORTB	&= 0b1100; // MUX: 00
	int recv_data = 0;
	PORTA &= ~0b100; // SS low to initate
	for (int ea_bit = 0b10000000; ea_bit > 0; ea_bit>>=1) {
		PORTA &= ~0b1000;
		if (ea_bit & byte) { PORTA |= 0b10; }
		else {               PORTA &=~0b10; }
		PORTA |= 0b1000;
		if (PINA & 0b1) { recv_data |= ea_bit; }
	}
	PORTA |= 0b100;
	return recv_data;
}
