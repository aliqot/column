#ifndef MODPORT_H
#define MODPORT_H

void MODPORT_init(void);

char MODPORT_contact_minion(char byte);
int MODPORT_is_minion_ready();

int MODPORT_add_to_queue(char byte);

int MODPORT_is_poppable();
int MODPORT_pop();
#endif
