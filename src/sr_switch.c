#include "sr_switch.h"

#include <avr/io.h>

char read_switch() {
	PORTA	&= ~0b1000;
	PORTB	|= 0b0010;
	PORTB	&= 0b1110;
	int recv_data = 0;
	PORTA &= ~0b100; // SS low to initate
	PORTA |= 0b100;
	for (int ea_bit = 0b1; ea_bit <= 0b10000000; ea_bit<<=1) {
//	for (int ea_bit = 0b10000000; ea_bit > 0; ea_bit>>=1) {
		if (PINA & 0b1) { recv_data |= ea_bit; }
		PORTA |= 0b1000;
		PORTA &= ~0b1000;
	}
	PORTA |= 0b1000;
	recv_data >>= 4;
//	recv_data &= 0b1111;
	return recv_data;
}

